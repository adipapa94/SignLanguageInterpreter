import mediapipe as mp

class HolisticPerceptor:
    
    def __init__(self):
        self.mp_holistic = mp.solutions.holistic
        self.mp_drawing = mp.solutions.drawing_utils

    def holisticDetector(self):
        return self.mp_holistic.Holistic(min_detection_confidence = 0.5, min_tracking_confidence = 0.5)
    
    def draw_landmarks(self, image, results):
        self.mp_drawing.draw_landmarks(image, results.face_landmarks, self.mp_holistic.FACEMESH_TESSELATION)
        self.mp_drawing.draw_landmarks(image, results.pose_landmarks, self.mp_holistic.POSE_CONNECTIONS)
        self.mp_drawing.draw_landmarks(image, results.left_hand_landmarks, self.mp_holistic.HAND_CONNECTIONS)
        self.mp_drawing.draw_landmarks(image, results.right_hand_landmarks, self.mp_holistic.HAND_CONNECTIONS)

    def drawStyledLandmarks(self, image, results):
        self.mp_drawing.draw_landmarks(image, results.face_landmarks, self.mp_holistic.FACEMESH_TESSELATION, self.mp_drawing.DrawingSpec(color = (80, 110, 10), thickness = 1, circle_radius = 1), self.mp_drawing.DrawingSpec(color = (80, 265, 121), thickness = 1, circle_radius = 1))
        self.mp_drawing.draw_landmarks(image, results.pose_landmarks, self.mp_holistic.POSE_CONNECTIONS, self.mp_drawing.DrawingSpec(color = (80, 110, 10), thickness = 1, circle_radius = 1), self.mp_drawing.DrawingSpec(color = (80, 265, 121), thickness = 1, circle_radius = 1))
        self.mp_drawing.draw_landmarks(image, results.left_hand_landmarks, self.mp_holistic.HAND_CONNECTIONS, self.mp_drawing.DrawingSpec(color = (80, 110, 10), thickness = 1, circle_radius = 1), self.mp_drawing.DrawingSpec(color = (80, 265, 121), thickness = 1, circle_radius = 1))
        self.mp_drawing.draw_landmarks(image, results.right_hand_landmarks, self.mp_holistic.HAND_CONNECTIONS, self.mp_drawing.DrawingSpec(color = (80, 110, 10), thickness = 1, circle_radius = 1), self.mp_drawing.DrawingSpec(color = (80, 265, 121), thickness = 1, circle_radius = 1))