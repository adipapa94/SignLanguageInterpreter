import os
import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Dense
from tensorflow.keras.callbacks import TensorBoard

LOG_PATH = ".\\log"
MODEL_PATH = ".\\model"

class Interpreter:

    def __init__(self):
        
        self.signs = ["hello", 'iloveyou', 'please', 'thanks']
        self.label_map = {label:num for num, label in enumerate(self.signs)}
        self.model = Sequential()

    def buildModel(self):
        self.model.add(LSTM(64, return_sequences = True, activation = "relu", input_shape = (30, 126)))
        self.model.add(LSTM(128, return_sequences = True, activation = "relu"))
        self.model.add(LSTM(64, return_sequences = False, activation = "relu"))
        self.model.add(Dense(64, activation = "relu"))
        self.model.add(Dense(32, activation = "relu"))
        self.model.add(Dense(self.signs.shape[0], activation = "softmax"))


    def trainModel(self, x_train, y_train, epochs = 250):
        log_dir = os.path.join(LOG_PATH, "Logs")
        tb_callback = TensorBoard(log_dir = log_dir)

        self.model.compile(optimizer = "Adam", loss = "categorical_crossentropy", metrics = ["categorical_accuracy"])
        self.model.fit(x_train, y_train, epochs = epochs, callbacks = [tb_callback])
        self.model.save(os.path.join(MODEL_PATH, "sign{}.h5".format(epochs)))
        self.model.summary()

    def testModel(self, x_test, y_test, model_path = None):
        if model_path:
            self.model.load_weights(model_path)
        # else:
        #     self.model.load_weights(os.path.join(MODEL_PATH, "signs{}.h5".format(epoch)))

        res = self.model.predict(x_test)

        for i in range(len(res)):
            prediction = self.signs[np.argmax(res[i])]
            actual = self.signs[np.argmax(y_test[i])]
            print("Current prediction is: {}\tActual value: {}".format(prediction, actual))


    def predict(self, observation):
        res = self.model.predict(observation)

    def TransferLearning(model_weights, training_set):
        model = Sequential()
        model.load_weights(model_weights)
