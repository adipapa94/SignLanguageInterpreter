# Sign Language Interpreter
import os
import cv2
import numpy as np
import mediapipe as mp

from InterpreterModel import Interpreter
from DataExtractor import DataExtractor

CAPTURE = False
TRAIN = False
TEST = True

DATA_PATH = ".\\data_30"
    
def mediapipe_detection(image, model):

    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image.flags.writeable = False

    results = model.process(image)

    image.flags.writeable = True
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    
    return image, results

# Capture data for training and testing for each of the signs

# Preparing data    


        
# Creating train, test sets
# epochs = [50, 100, 150, 200, 250, 300, 350, 400]
epochs = [250]


for epoch in epochs:
    # Creating the Neural Network
    model = Sequential()

    if TEST:
        
sequence    = []
sentence    = []
guesses     = []
no_of_guesses = 0
treshold    = 0.8
model_to_load = epochs[0]

model.load_weights(os.path.join(MODEL_PATH, "signs{}.h5".format(model_to_load)))
with mp_holistic.Holistic(min_detection_confidence = 0.5, min_tracking_confidence = 0.5) as holistic:

    while cap.isOpened():
        ret, frame = cap.read()
        
        # Detection of body parts
        image, results = mediapipe_detection(frame, holistic)
        
        # Draw Landmarks
        drawStyledLandmarks(image, results)
        
        keyPoints = extractKeyPoints(results)
        sequence.insert(0, keyPoints)
        sequence = sequence[:30]
        
        if len(sequence) == 30:
            res = model.predict(np.expand_dims(sequence, axis = 0))[0]
            if res[np.argmax(res)] > treshold:
                print(signs[np.argmax(res)])
            guesses.append(np.argmax(res))
            no_of_guesses += 1

        if no_of_guesses > 10:
            if np.unique(guesses[-10:])[0] == np.argmax(res):
                # if res[np.argmax(res)] > treshold:
                cv2.putText(image, "{}".format(signs[np.argmax(res)]), (3, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), 2, cv2.LINE_AA)
            else:
                cv2.putText(image, "No sign detected", (3, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), 2, cv2.LINE_AA)
                    
        
        cv2.imshow("Sign Language Interpreter", image)
        
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break
    
    cap.release()
    cv2.destroyAllWindows()