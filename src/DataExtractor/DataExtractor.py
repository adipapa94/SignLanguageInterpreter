import os
import cv2
import numpy as np
from HolisticPerceptor import HolisticPerceptor
from sklearn.model_selection import train_test_split
from tensorflow.keras.utils import to_categorical

DATA_PATH = ".\\data"

class DataExtractor:
    
    def __init__(self):
        self.detector = HolisticPerceptor()
        self.sequence_length = 30
        self.sequences = []
        self.labels = []
        self.no_sequences = 60

    def capture(self, signs):

        cap = cv2.VideoCapture(0)

        for sign in signs:
            for sequence in range(self.no_sequences):
                try:
                    os.makedirs(os.path.join(DATA_PATH, sign, str(sequence)))
                except:
                    pass

        with self.detector.holisticDetector() as holistic:

            for sign in signs:
                for sequence in range(self.no_sequences):
                    for frame_num in range(self.sequence_length):

                        ret, frame = cap.read()
                        
                        # Detection of body parts
                        image, results = self.mediapipe_detection(frame, holistic)
                        
                        # Draw Landmarks
                        self.detector.drawStyledLandmarks(image, results)

                        # Apply wait logic
                        if frame_num == 0:
                            cv2.putText(image, "Starting Collection", (120, 200), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 4, cv2.LINE_AA)
                            cv2.putText(image, "Collecting frames for {} Video number: {}".format(sign, sequence), (15, 12), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
                            cv2.imshow("OpenCV Feed", image)
                            cv2.waitKey(2000)
                        else:
                            cv2.putText(image, "Collecting frames for {} Video number: {}".format(sign, sequence), (15, 12), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
                            cv2.imshow("OpenCV Feed", image)

                        keyPoints = self.extractKeyPoints(results)
                        npy_path = os.path.join(DATA_PATH, sign, str(sequence), str(frame_num))
                        np.save(npy_path, keyPoints)
                        
                        
                        if cv2.waitKey(10) & 0xFF == ord('q'):
                            break
        
        cap.release()
        cv2.destroyAllWindows()

    def prepareData(self):
        
        for sign in self.signs:
            for sequence in range(self.no_sequences):
                window = []
                
                for frame_num in range(self.sequence_length):
                    res = np.load(os.path.join(DATA_PATH, sign, str(sequence), "{}.npy".format(frame_num)))
                    window.append(res)
                    
                self.sequences.append(window)
                self.labels.append(self.label_map[sign])

    def getTrainTestData(self):
        x = np.array(self.sequences)
        y = to_categorical(self.labels).astype(int)

        print("X is: {}\nY is {}".format(x,y))
        print("Shape of X: {}\nShape of Y: {}".format(x.shape, y.shape))

        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.4)
        print("X test: {}\nY test: {}".format(x_test, y_test))

        return x_train, x_test, y_train, y_test

    def extractKeyPoints(self, results):
        # pose = np.array([[res.x, res.y, res.z, res.visibility] for res in results.pose_landmarks.landmark]).flatten() if results.pose_landmarks else np.zeros(132)
        # face = np.array([[res.x, res.y, res.z] for res in results.face_landmarks.landmark]).flatten() if results.face_landmarks else np.zeros(1404)
        lh = np.array([[res.x, res.y, res.z] for res in results.left_hand_landmarks.landmark]).flatten() if results.left_hand_landmarks else np.zeros(21*3)
        rh = np.array([[res.x, res.y, res.z] for res in results.right_hand_landmarks.landmark]).flatten()if results.right_hand_landmarks else np.zeros(21*3)
        
        return np.concatenate([lh, rh]) 
